# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "1.0.0"

import json
import os
import threading
from . import constants
from amqpstorm import Message
from datetime import datetime, timezone
from minty.cqrs import Event, MiddlewareBase
from minty.exceptions import ConfigurationConflict
from sqlalchemy.sql import and_, join, select
from uuid import UUID, uuid4
from zsnl_domains.database import schema

DEFAULT_EXCHANGE = os.environ.get("MQ_EXCHANGE", "amq.topic")

CACHE = threading.local()


def get_queue_item_cache():
    """Retrieve the (thread local) UUID cache.
    The UUID cache is used to share generated UUIDs of queue-items
    between the _LegacyEventQueueMiddleware and _LegacyEventBroadcastMiddleware

    :return: Cache with uuid attribute.
    :rtype List
    """
    try:
        queue_item_cache = CACHE.queue_items
    except AttributeError:
        clear_queue_item_cache()
        queue_item_cache = CACHE.queue_items

    return queue_item_cache


def clear_queue_item_cache():
    CACHE.queue_items = []


def get_new_values_from_event(event: Event):
    """Return a dictionary of the new values of an entity.

    :param event: An event to extract all the "new values" from.
    :type changes: Event
    :return: A dictionary with key/value pairs representing the parts of the
        entity that were changed and the new values.
    :rtype: dict
    """
    new_values = {}
    for change in event.changes:
        new_values[change["key"]] = change["new_value"]
    return new_values


def is_previewable(mimetype, extension):
    """Return whether a document is previewable, based on MIME type and extension

    :param mimetype: The MIME type of the document being created/uploaded
    :param extension: The extension of the document being created/uploaded. Not always present in the created document.
    :return: If the document being uploaded is previewable.
    :rtype Bool
    """
    if mimetype.startswith("audio/") or mimetype.startswith("video/"):
        return False

    if (mimetype,) in constants.NON_PDF_PREVIEWABLE_MIME_TYPES:
        return False

    if (mimetype, extension) in constants.NON_PDF_PREVIEWABLE_MIME_TYPES:
        return False

    return True


def select_interesting_objects(
    interesting_events,
    interesting_casetype_events,
    interesting_document_events,
    event_list,
    interesting_case_relation_events=None,
    interesting_subject_relation_events=None,
):
    events_to_handle = {
        "case_touches": {},
        "case_type_touches": {},
        "document_tasks": {},
        "case_relations": {},
        "subject_relations": {},
    }

    case_touches = events_to_handle["case_touches"]
    case_type_touches = events_to_handle["case_type_touches"]
    document_tasks = events_to_handle["document_tasks"]
    case_relations = events_to_handle["case_relations"]
    subject_relations = events_to_handle["subject_relations"]

    interesting_case_relation_events = interesting_case_relation_events or []
    interesting_subject_relation_events = (
        interesting_subject_relation_events or []
    )

    for event in event_list:
        if (
            (event.domain, event.event_name)
            in interesting_case_relation_events
            and event.entity_id not in case_relations
        ):
            # This is a special case because the entity_id does not represent a
            # case, so we have to extract the two case ids from the event changes
            case_relations[event.entity_id] = event
        elif (
            event.domain,
            event.event_name,
        ) in interesting_events and event.entity_id not in case_touches:
            case_touches[event.entity_id] = event
        elif (
            (event.domain, event.event_name) in interesting_casetype_events
            and event.entity_id not in case_type_touches
        ):
            case_type_touches[event.entity_id] = event
        elif (
            (event.domain, event.event_name) in interesting_document_events
            and event.entity_id not in document_tasks
        ):
            document_tasks[event.entity_id] = event
        if (
            (event.domain, event.event_name)
            in interesting_subject_relation_events
            and event.entity_id not in subject_relations
        ):
            subject_relations[event.entity_id] = event

    return events_to_handle


class LegacyEventQueueMiddlewareBase(MiddlewareBase):
    def _get_database(self, context):
        return self.infrastructure_factory.get_infrastructure(
            infrastructure_name=self.database_infra_name, context=context
        )

    def _touch_case_relation(self, event: Event):
        changes = {}
        for change in event.changes:
            changes[change["key"]] = change["new_value"]

        queue_item_cache = get_queue_item_cache()

        # Perl will create rows for both cases,
        # duplicating the rows in the relation table,
        # so only one case touch must be done
        case_uuid = changes["current_case_uuid"]
        queue_item_uuid1 = uuid4()

        self.insert_queue_item(
            event_context=event.context,
            event_uuid=queue_item_uuid1,
            object_id=case_uuid,
            data={"case_object_id": case_uuid},
            event_type="update_object_relationships",
            label="Case relation update",
        )
        queue_item_cache.append(
            {
                "uuid": str(queue_item_uuid1),
                "context": event.context,
                "event_type": "update_object_relationships",
            }
        )

        # Touch both cases in relationship
        first_case_uuid = changes.get(
            "object1_uuid", None
        ) or event.entity_data.get("object1_uuid", None)
        second_case_uuid = changes.get(
            "object2_uuid", None
        ) or event.entity_data.get("object2_uuid", None)

        # For events like re-ordering, no need of case_touch
        # Below check will prevent unwanted case_touches
        if first_case_uuid:
            queue_item_uuid2 = uuid4()
            self.insert_queue_item(
                event_context=event.context,
                event_uuid=queue_item_uuid2,
                object_id=None,
                data={"case_object_id": str(first_case_uuid)},
                event_type="touch_case",
                label="Legacy zaak synchronization",
            )
            queue_item_cache.append(
                {
                    "uuid": str(queue_item_uuid2),
                    "context": event.context,
                    "event_type": "touch_case",
                }
            )
        if second_case_uuid:
            queue_item_uuid3 = uuid4()
            self.insert_queue_item(
                event_context=event.context,
                event_uuid=queue_item_uuid3,
                object_id=None,
                data={"case_object_id": str(second_case_uuid)},
                event_type="touch_case",
                label="Legacy zaak synchronization",
            )
            queue_item_cache.append(
                {
                    "uuid": str(queue_item_uuid3),
                    "context": event.context,
                    "event_type": "touch_case",
                }
            )

    def _touch_case(self, event: Event):
        self.insert_queue_item(
            event_context=event.context,
            event_uuid=event.uuid,
            object_id=None,
            data={"case_object_id": str(event.entity_id)},
            event_type="touch_case",
            label="Legacy zaak synchronization",
        )

    def _touch_casetype(self, event_uuid, context: str, casetype_id: UUID):
        self.insert_queue_item(
            event_context=context,
            event_uuid=event_uuid,
            object_id=str(casetype_id),
            data={"uuid": str(casetype_id)},
            event_type="touch_casetype",
            label="Legacy case type synchronization",
        )

    def _process_casetype(self, event: Event):
        """Process a "case type touch" interesting event

        Some "case type touch" events are triggered by events that aren't
        on the CaseType entity."""

        if event.entity_type in [
            "Attribute",
            "DocumentTemplate",
            "EmailTemplate",
        ]:
            case_type_ids = self._get_case_types_for_event(event)
        elif event.entity_type == "CaseTypeEntity":
            case_type_ids = [event.entity_id]
        else:
            raise ConfigurationConflict(
                "Unknown CaseType touch event", "legacy_touch/unknown_event"
            )

        queue_item_cache = get_queue_item_cache()

        for case_type_id in case_type_ids:
            queue_item_uuid = uuid4()
            self._touch_casetype(queue_item_uuid, event.context, case_type_id)
            queue_item_cache.append(
                {
                    "uuid": str(queue_item_uuid),
                    "context": event.context,
                    "event_type": "touch_casetype",
                }
            )

    def _process_document(self, event: Event):
        """Send out old-style queue items for document events"""

        if event.entity_type == "File":
            return

        new_values = get_new_values_from_event(event)
        file = self.get_file(event)
        queue_item_cache = get_queue_item_cache()

        if new_values.get("case_uuid"):
            # If documents belong to a case, that case needs to be touched
            self.insert_queue_item(
                event_context=event.context,
                event_uuid=event.uuid,
                object_id=None,  # touch should not show a "queued tasks" popup
                data={"case_object_id": str(new_values["case_uuid"])},
                event_type="touch_case",
                label="Legacy zaak synchronization",
            )
            queue_item_cache.append(  # caching the queue item so that it will be published on "self.publish_cached_events"
                {
                    "uuid": event.uuid,
                    "context": event.context,
                    "event_type": "touch_case",
                }
            )

        if new_values.get("mimetype") and is_previewable(
            new_values.get("mimetype"), file["extension"]
        ):
            queue_item_uuid = uuid4()
            self.insert_queue_item(
                event_uuid=queue_item_uuid,
                event_context=event.context,
                event_type="generate_preview_pdf",
                label=f"Generate preview PDF for file {event.entity_id}",
                data={"file_id": file["id"]},
                object_id=None,
            )
            queue_item_cache.append(
                {
                    "uuid": str(queue_item_uuid),
                    "context": event.context,
                    "event_type": "generate_preview_pdf",
                }
            )

        if file["extension"] not in [".mime", ".eml", ".msg"]:
            queue_item_uuid = uuid4()
            self.insert_queue_item(
                event_uuid=queue_item_uuid,
                event_context=event.context,
                event_type="set_search_terms",
                label=f"Extract search terms for file {event.entity_id}",
                data={"file_id": file["id"]},
                object_id=None,
            )
            queue_item_cache.append(
                {
                    "uuid": str(queue_item_uuid),
                    "context": event.context,
                    "event_type": "set_search_terms",
                }
            )

        if (
            new_values.get("mimetype")
            and new_values.get("mimetype") == "application/pdf"
        ):
            queue_item_uuid = uuid4()
            self.insert_queue_item(
                event_uuid=queue_item_uuid,
                event_context=event.context,
                event_type="generate_thumbnail",
                label=f"Generate thumbnail for file {event.entity_id}",
                data={"file_id": file["id"]},
                object_id=None,
            )
            queue_item_cache.append(
                {
                    "uuid": str(queue_item_uuid),
                    "context": event.context,
                    "event_type": "generate_thumbnail",
                }
            )

    def _touch_subject_relation(self, event: Event):
        queue_item_cache = get_queue_item_cache()

        case_uuid = event.entity_data["case"]["id"]

        acl_queue_item_uuid = uuid4()
        self.insert_queue_item(
            event_context=event.context,
            event_uuid=acl_queue_item_uuid,
            object_id=case_uuid,
            data={"case_object_id": case_uuid},
            event_type="update_object_acl",
            label="Legacy zaak ACL sync for changed subject_relation",
        )
        queue_item_cache.append(
            {
                "uuid": str(acl_queue_item_uuid),
                "context": event.context,
                "event_type": "update_object_acl",
            }
        )

        case_update_system_attribute_uuid = uuid4()
        self.insert_queue_item(
            event_context=event.context,
            event_uuid=case_update_system_attribute_uuid,
            object_id=None,
            data={
                "case_object_id": str(case_uuid),
                "attributes": [
                    {"name": "coordinator", "is_group": True},
                    {"name": "recipient", "is_group": True},
                ],
            },
            event_type="case_update_system_attribute",
            label="Legacy zaak synchronization",
        )
        queue_item_cache.append(
            {
                "uuid": str(case_update_system_attribute_uuid),
                "context": event.context,
                "event_type": "case_update_system_attribute",
            }
        )

    def insert_queue_item(
        self,
        event_context: str,
        event_uuid: UUID,
        event_type,
        object_id,
        data: dict,
        label,
    ):
        queue_item = schema.Queue(
            id=event_uuid,
            object_id=object_id,
            status="pending",
            type=event_type,
            label=label,
            data=data,
            priority=1000,
            date_created=datetime.now(timezone.utc),
            _metadata={
                "disable_acl": 1,
                "require_object_model": 1,
                "target": "backend",
            },
        )

        session = self._get_database(event_context)
        session.add(queue_item)

    def get_file(self, event):
        """Retrieve file id,extension based on the event."""

        session = self._get_database(event.context)
        query = select([schema.File.id, schema.File.extension]).where(
            schema.File.uuid == event.entity_id
        )
        result = session.execute(query).fetchone()
        return {"id": result.id, "extension": result.extension}

    def _get_case_types_for_event(self, event: Event):
        session = self._get_database(event.context)

        joined_tables = join(
            schema.ObjectData,
            schema.Zaaktype,
            and_(
                schema.ObjectData.object_class == "casetype",
                schema.ObjectData.object_id == schema.Zaaktype.id,
            ),
        ).join(
            schema.ZaaktypeNode,
            schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
        )

        if event.entity_type == "Attribute":
            joined_tables = joined_tables.join(
                schema.ZaaktypeKenmerk,
                schema.ZaaktypeNode.id
                == schema.ZaaktypeKenmerk.zaaktype_node_id,
            ).join(
                schema.BibliotheekKenmerk,
                schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
                == schema.BibliotheekKenmerk.id,
            )
            query = (
                select([schema.ObjectData.uuid])
                .select_from(joined_tables)
                .where(schema.BibliotheekKenmerk.uuid == event.entity_id)
            )
        elif event.entity_type == "DocumentTemplate":
            joined_tables = joined_tables.join(
                schema.ZaaktypeSjabloon,
                schema.ZaaktypeNode.id
                == schema.ZaaktypeSjabloon.zaaktype_node_id,
            ).join(
                schema.BibliotheekSjabloon,
                schema.ZaaktypeSjabloon.bibliotheek_sjablonen_id
                == schema.BibliotheekSjabloon.id,
            )
            query = (
                select([schema.ObjectData.uuid])
                .select_from(joined_tables)
                .where(schema.BibliotheekSjabloon.uuid == event.entity_id)
            )
        elif event.entity_type == "EmailTemplate":
            joined_tables = joined_tables.join(
                schema.ZaaktypeNotificatie,
                schema.ZaaktypeNode.id
                == schema.ZaaktypeNotificatie.zaaktype_node_id,
            ).join(
                schema.BibliotheekNotificatie,
                schema.ZaaktypeNotificatie.bibliotheek_notificatie_id
                == schema.BibliotheekNotificatie.id,
            )
            query = (
                select([schema.ObjectData.uuid])
                .select_from(joined_tables)
                .where(schema.BibliotheekNotificatie.uuid == event.entity_id)
            )

        result = session.execute(query).fetchall()

        return [r.uuid for r in result]


def LegacyEventQueueMiddleware(
    database_infra_name: str,
    interesting_events: list = None,
    interesting_casetype_events: list = None,
    interesting_document_events: list = None,
    interesting_case_relation_events: list = None,
    interesting_subject_relation_events: list = None,
):
    """Save "case(type)_touch" to the legacy Zaaksysteem "Queue" table.

    Inserts a new queue-item into the legacy Zaaksysteem queue table if the
    current event requires a "touch case" or "touch case type" to be executed.

    The uuid of the queue item will be the same as the uuid of the event.
    """

    interesting_events = interesting_events or []
    interesting_casetype_events = interesting_casetype_events or []
    interesting_document_events = interesting_document_events or []
    interesting_case_relation_events = interesting_case_relation_events or []
    interesting_subject_relation_events = (
        interesting_subject_relation_events or []
    )

    class _LegacyEventQueueMiddleware(LegacyEventQueueMiddlewareBase):
        def __call__(self, func):
            """Insert a "Queue" row into the Zaaksysteem database

            :param func: Next function in the middleware stack to call
            :type func: callable
            """
            func()

            self.database_infra_name = database_infra_name

            objects_to_touch = select_interesting_objects(
                interesting_events=interesting_events,
                interesting_casetype_events=interesting_casetype_events,
                interesting_document_events=interesting_document_events,
                event_list=self.event_service.event_list,
                interesting_case_relation_events=interesting_case_relation_events,
                interesting_subject_relation_events=interesting_subject_relation_events,
            )

            for event in objects_to_touch["case_touches"].values():
                self._touch_case(event)
            for event in objects_to_touch["case_type_touches"].values():
                self._process_casetype(event)
            for event in objects_to_touch["document_tasks"].values():
                self._process_document(event)
            for event in objects_to_touch["case_relations"].values():
                self._touch_case_relation(event)
            for event in objects_to_touch["subject_relations"].values():
                self._touch_subject_relation(event)

    return _LegacyEventQueueMiddleware


class LegacyEventBroadcastMiddlewareBase(MiddlewareBase):
    def publish_event(
        self, routing_key_suffix: str, context: str, logging_id, data: dict
    ) -> None:
        raise NotImplementedError

    def publish_cached_events(self):
        # Events stored in the database
        for queue_item in get_queue_item_cache():
            context = queue_item["context"]
            queue_item_uuid = queue_item["uuid"]

            config = self.infrastructure_factory.get_config(context)
            logging_id = config["logging_id"]
            data = {
                "url": (
                    f"https://{context}/api/queue/{queue_item_uuid}/run"
                    + f"?type={queue_item['event_type']}"
                ),
                "instance_hostname": context,
            }

            self.publish_event(
                routing_key_suffix=queue_item["event_type"],
                context=context,
                logging_id=logging_id,
                data=data,
            )

    def publish_document_events(self, event: Event):
        new_values = get_new_values_from_event(event)

        config = self.infrastructure_factory.get_config(event.context)

        logging_id = config["logging_id"]
        virus_scan_url = config["virus_scanner_url"]

        try:
            storage_uuid = new_values["store_uuid"]
        except KeyError:
            storage_uuid = str(event.entity_id)

        # Virus scan (not stored in the database, only broadcast)
        self.publish_event(
            routing_key_suffix="virus_scan",
            context=event.context,
            data={
                "url": f"{virus_scan_url}/filestore",
                "instance_hostname": event.context,
                "file_id": storage_uuid,
            },
            logging_id=logging_id,
        )


def LegacyEventBroadcastMiddleware(
    amqp_infra_name: str,
    interesting_events: list = None,
    interesting_casetype_events: list = None,
    interesting_document_events: list = None,
    exchange_name: str = DEFAULT_EXCHANGE,
    message_class=Message,
    interesting_case_relation_events: list = None,
    interesting_subject_relation_events: list = None,
):
    """Broadcast a legacy queue event on the AMQP bus.

    Publishes a queue-item on the legacy RabbitMQ bus if the current event
    requires a "touch case" or "touch case type" to be executed.

    The uuid of the queue item will be the same as the uuid of the queue item.
    """
    interesting_events = interesting_events or []
    interesting_casetype_events = interesting_casetype_events or []
    interesting_document_events = interesting_document_events or []
    interesting_case_relation_events = interesting_case_relation_events or []
    interesting_subject_relation_events = (
        interesting_subject_relation_events or []
    )

    class _LegacyEventBroadcastMiddleware(LegacyEventBroadcastMiddlewareBase):
        def __call__(self, func):
            """Broadcast the queue-item to the legacy RabbitMQ queue

            :param func: Function
            :type func: callable
            :param event: event
            :type event: Event
            """
            func()

            events_to_touch = select_interesting_objects(
                interesting_events=interesting_events,
                interesting_casetype_events=interesting_casetype_events,
                interesting_document_events=interesting_document_events,
                event_list=self.event_service.event_list,
                interesting_case_relation_events=interesting_case_relation_events,
                interesting_subject_relation_events=interesting_subject_relation_events,
            )

            for e in events_to_touch["case_touches"].values():
                config = self.infrastructure_factory.get_config(e.context)
                logging_id = config["logging_id"]

                self.publish_event(
                    routing_key_suffix="touch_case",
                    context=e.context,
                    logging_id=logging_id,
                    data={
                        "url": f"https://{e.context}/api/queue/{e.uuid}/run?type=touch_case",
                        "instance_hostname": e.context,
                    },
                )
            for e in events_to_touch["document_tasks"].values():
                # Publish multiple events related to a document
                # (virus scan, search terms, etc.)
                self.publish_document_events(e)

            # events_to_touch["case_type_events"] is not checked, because those
            # all end up in the cached event list:
            self.publish_cached_events()

            clear_queue_item_cache()

        def publish_event(
            self, routing_key_suffix: str, context: str, logging_id, data: dict
        ):
            """Publish legacy events on the AMQP bus.

            :param routing_key_suffix:
            :type routing_key_suffix: str
            :param context:
            :type context: str
            :param logging_id
            :type logging_id: str
            :param data
            :type data: dict
            :return: void
            """
            amqp_channel = self.infrastructure_factory.get_infrastructure(
                infrastructure_name=amqp_infra_name, context=context
            )
            mq_message = json.dumps(data, sort_keys=True)

            message = message_class(channel=amqp_channel, body=mq_message)
            routing_key = f"zs.v0.{logging_id}.{routing_key_suffix}"
            self.logger.info(
                f"Publishing a queue-item to {exchange_name}: routing_key={routing_key}"
            )

            timer = self.statsd.get_timer("amqp_write_duration")
            counter = self.statsd.get_counter("amqp_write_number")
            with timer.time():
                message.publish(
                    routing_key=routing_key,
                    exchange=exchange_name,
                )
            counter.increment()

    return _LegacyEventBroadcastMiddleware
